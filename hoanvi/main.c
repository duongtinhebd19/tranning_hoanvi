#include <stdio.h>
#include <stdlib.h>

// bai tap: nhap vào số n và tính hoán vị của n;

//hoan vi: n! = 1*2*3*...*n | quy uoc 0! = 1

int main()
{
    int n = 0; // xin trong ram 1 vung nho 4 byte va set gia tri = 0
    int giaTriHoanVi = 1;
    printf("Vui long nhap vao 1 so: \n");
    scanf("%d", &n);
    printf("Gia tri nhap vao la: %d\n", n);

    if(n == 0) {
        printf("Gia tri hoan vi cua %d la: %d\n", n, 1);
    }
    else {
        // i++ == i + 1;
        for(int i = 0; i < n; i++) {
            giaTriHoanVi = giaTriHoanVi*(i+1);
        }
        printf("Gia tri hoan vi cua %d la: %d\n", n, giaTriHoanVi);
    }
    return 0;
}
